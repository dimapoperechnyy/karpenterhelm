# karpenterhelm

This Chart installs an ec2nodeclass, as well as a nodepool CRD on your cluster. 

This is needed for Karpenter to work properly.

Required variables to pass in are:

  - clusterName
  - kmsKeyID
  - role (this is the node iam role)

